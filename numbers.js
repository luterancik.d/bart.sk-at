const numbers = {
    phoneNrsAreEqual: (num1,num2) => {

        num1 = num1.replace(/\s/g, '');
        num2 = num2.replace(/\s/g, '');

        num1 = num1.replace(/-/g, '');
        num2 = num2.replace(/-/g, '');

        num1 = num1.replace(/\//g, "");
        num2 = num2.replace(/\//g, "");

        num1 = num1.replace(/\+/g, "");
        num2 = num2.replace(/\+/g, "");

        num1 = num1.replace(/\(/g, "");
        num2 = num2.replace(/\(/g, "");

        num1 = num1.replace(/\)/g, "");
        num2 = num2.replace(/\)/g, "");

        num1 = num1.replace(/(00421|421|0)/, "");
        num2 = num2.replace(/(00421|421|0)/, "");
        
        if( num1 === num2) return true
        else 
        return false
    }
}

module.exports = numbers;