

describe('Bart.sk/mam-zaujem', function () {

    it('Sucessfully send a complete contact form', function() {

       cy.viewport(1920, 937)

       cy.visit('https://www.bart.sk/mam-zaujem-test')

       cy.get('.row > #contact-form > .col-3 > #form-group-name > #name').click()

       cy.get('.row > #contact-form > .col-3 > #form-group-name > #name').type('Ahmed').should('have.value','Ahmed')

       cy.get('.row > #contact-form > .col-3 > #form-group-company > #company-name').type('Nakadi').should('have.value','Nakadi')

       cy.get('.row > #contact-form > .col-3 > #form-group-email > #email').type('ahmed.nakadi@test.com').should('have.value','ahmed.nakadi@test.com')

       cy.get('.row > #contact-form > .col-3 > #form-group-tel > #tel').type('0909123456').should('have.value','0909123456')

       cy.get('#contact-form > .col-3 > #form-group-interest > label:nth-child(14) > input').check('Web stránka').should('have.value','Web stránka')

       cy.get('.row > #contact-form > .col-6 > #form-group-message > #message').type('Automated test').should('have.value','Automated test')

       cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')

       cy.get('.content > .row > #contact-form > .col-12 > #contact-submit').click()
       cy.wait(10000)

       cy.get('body > #modal-overlay-contact-us > .modal-box').should('be.visible')

       cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')
    })

    it('Missing name - incomplete contact form not sent', function () {
        cy.viewport(1920, 937)

        cy.visit('https://www.bart.sk/mam-zaujem-test')

        cy.get('.row > #contact-form > .col-3 > #form-group-name > #name').should('have.value', '')
        cy.get('.row > #contact-form > .col-3 > #form-group-name > .error-text').should('not.exist')

        cy.get('.row > #contact-form > .col-3 > #form-group-company > #company-name').type('Nakadi').should('have.value', 'Nakadi')

        cy.get('.row > #contact-form > .col-3 > #form-group-email > #email').type('ahmed.nakadi@test.com').should('have.value', 'ahmed.nakadi@test.com')

        cy.get('.row > #contact-form > .col-3 > #form-group-tel > #tel').type('0909123456').should('have.value', '0909123456')

        cy.get('#contact-form > .col-3 > #form-group-interest > label:nth-child(14) > input').check('Web stránka').should('have.value', 'Web stránka')

        cy.get('.row > #contact-form > .col-6 > #form-group-message > #message').type('Automated test').should('have.value', 'Automated test')

        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')

        cy.get('.content > .row > #contact-form > .col-12 > #contact-submit').click()
        cy.wait(10000)
        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')
        cy.get('.row > #contact-form > .col-3 > #form-group-name > .error-text').should('exist')

    })


    it('Missing company - incomplete contact form not sent', function () {
        cy.viewport(1920, 937)

        cy.visit('https://www.bart.sk/mam-zaujem-test')

        cy.get('.row > #contact-form > .col-3 > #form-group-name > #name').type('Achmed').should('have.value', 'Achmed')


        cy.get('.row > #contact-form > .col-3 > #form-group-company > #company-name').should('have.value', '')
        cy.get('.row > #contact-form > .col-3 > #form-group-company > .error-text').should('not.exist')

        cy.get('.row > #contact-form > .col-3 > #form-group-email > #email').type('ahmed.nakadi@test.com').should('have.value', 'ahmed.nakadi@test.com')

        cy.get('.row > #contact-form > .col-3 > #form-group-tel > #tel').type('0909123456').should('have.value', '0909123456')

        cy.get('#contact-form > .col-3 > #form-group-interest > label:nth-child(14) > input').check('Web stránka').should('have.value', 'Web stránka')

        cy.get('.row > #contact-form > .col-6 > #form-group-message > #message').type('Automated test').should('have.value', 'Automated test')

        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')

        cy.get('.content > .row > #contact-form > .col-12 > #contact-submit').click()
        cy.wait(10000)
        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')
        cy.get('.row > #contact-form > .col-3 > #form-group-company > .error-text').should('exist')

    })

    it('Missing email - incomplete contact form not sent', function () {
        cy.viewport(1920, 937)

        cy.visit('https://www.bart.sk/mam-zaujem-test')

        cy.get('.row > #contact-form > .col-3 > #form-group-name > #name').type('Achmed').should('have.value', 'Achmed')


        cy.get('.row > #contact-form > .col-3 > #form-group-company > #company-name').type('Nakadi').should('have.value', 'Nakadi')


        cy.get('.row > #contact-form > .col-3 > #form-group-email > #email').should('have.value', '')
        cy.get('.row > #contact-form > .col-3 > #form-group-email > .error-text').should('not.exist')

        cy.get('.row > #contact-form > .col-3 > #form-group-tel > #tel').type('0909123456').should('have.value', '0909123456')

        cy.get('#contact-form > .col-3 > #form-group-interest > label:nth-child(14) > input').check('Web stránka').should('have.value', 'Web stránka')

        cy.get('.row > #contact-form > .col-6 > #form-group-message > #message').type('Automated test').should('have.value', 'Automated test')

        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')

        cy.get('.content > .row > #contact-form > .col-12 > #contact-submit').click()
        cy.wait(10000)
        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')
        cy.get('.row > #contact-form > .col-3 > #form-group-email > .error-text').should('exist')

    })



    it('Missing telephone - incomplete contact form not sent', function () {
        cy.viewport(1920, 937)

        cy.visit('https://www.bart.sk/mam-zaujem-test')

        cy.get('.row > #contact-form > .col-3 > #form-group-name > #name').type('Achmed').should('have.value', 'Achmed')


        cy.get('.row > #contact-form > .col-3 > #form-group-company > #company-name').type('Nakadi').should('have.value', 'Nakadi')


        cy.get('.row > #contact-form > .col-3 > #form-group-email > #email').type('ahmed.nakadi@test.com').should('have.value', 'ahmed.nakadi@test.com')


        cy.get('.row > #contact-form > .col-3 > #form-group-tel > #tel').should('have.value', '')
        cy.get('.row > #contact-form > .col-3 > #form-group-tel > .error-text').should('not.exist')

        cy.get('#contact-form > .col-3 > #form-group-interest > label:nth-child(14) > input').check('Web stránka').should('have.value', 'Web stránka')

        cy.get('.row > #contact-form > .col-6 > #form-group-message > #message').type('Automated test').should('have.value', 'Automated test')

        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')

        cy.get('.content > .row > #contact-form > .col-12 > #contact-submit').click()
        cy.wait(10000)
        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')
        cy.get('.row > #contact-form > .col-3 > #form-group-tel > .error-text').should('exist')


    })

    it('Wrong email format - incomplete contact form not sent', function () {
        cy.viewport(1920, 937)

        cy.visit('https://www.bart.sk/mam-zaujem-test')

        cy.get('.row > #contact-form > .col-3 > #form-group-name > #name').type('Achmed').should('have.value', 'Achmed')


        cy.get('.row > #contact-form > .col-3 > #form-group-company > #company-name').type('Nakadi').should('have.value', 'Nakadi')


        cy.get('.row > #contact-form > .col-3 > #form-group-email > #email').type('abcdefg').should('have.value', 'abcdefg')
        cy.get('.row > #contact-form > .col-3 > #form-group-email > .error-text').should('not.exist')

        cy.get('.row > #contact-form > .col-3 > #form-group-tel > #tel').type('0909123456').should('have.value', '0909123456')

        cy.get('#contact-form > .col-3 > #form-group-interest > label:nth-child(14) > input').check('Web stránka').should('have.value', 'Web stránka')

        cy.get('.row > #contact-form > .col-6 > #form-group-message > #message').type('Automated test').should('have.value', 'Automated test')

        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')

        cy.get('.content > .row > #contact-form > .col-12 > #contact-submit').click()

        cy.wait(10000)
        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')
        cy.get('.row > #contact-form > .col-3 > #form-group-email > .error-text').should('exist')
    })



    it('Missing all - incomplete contact form not sent', function () {
        cy.viewport(1920, 937)

        cy.visit('https://www.bart.sk/mam-zaujem-test')

        cy.get('.row > #contact-form > .col-3 > #form-group-name > #name').should('have.value', '')
        cy.get('.row > #contact-form > .col-3 > #form-group-name > .error-text').should('not.exist')

        cy.get('.row > #contact-form > .col-3 > #form-group-company > #company-name').should('have.value', '')
        cy.get('.row > #contact-form > .col-3 > #form-group-company > .error-text').should('not.exist')

        cy.get('.row > #contact-form > .col-3 > #form-group-email > #email').should('have.value', '')
        cy.get('.row > #contact-form > .col-3 > #form-group-email > .error-text').should('not.exist')

        cy.get('.row > #contact-form > .col-3 > #form-group-tel > #tel').should('have.value', '')
        cy.get('.row > #contact-form > .col-3 > #form-group-tel > .error-text').should('not.exist')

        cy.get('#contact-form > .col-3 > #form-group-interest > label:nth-child(14) > input').should('not.be.checked')
        cy.get('#contact-form > .col-3 > #form-group-interest > .error-text').should('not.exist')

        cy.get('.row > #contact-form > .col-6 > #form-group-message > #message').should('have.value', '')
        cy.get('.row > #contact-form > .col-6 > #form-group-message > .error-text').should('not.exist')

        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')

        cy.get('.content > .row > #contact-form > .col-12 > #contact-submit').click()

        cy.wait(10000)

        cy.get('body > #modal-overlay-contact-us > .modal-box').should('not.be.visible')
        cy.get('.row > #contact-form > .col-3 > #form-group-name > .error-text').should('exist')
        cy.get('.row > #contact-form > .col-3 > #form-group-company > .error-text').should('exist')
        cy.get('.row > #contact-form > .col-3 > #form-group-email > .error-text').should('exist')
        cy.get('.row > #contact-form > .col-3 > #form-group-tel > .error-text').should('exist')
        cy.get('.row > #contact-form > .col-3 > #form-group-interest > .error-text').should('exist')
        cy.get('.row > #contact-form > .col-6 > #form-group-message > .error-text').should('exist')

    })

    // Wrong number format? eg. abc

    // No checkbox selected as in "Missing all"

    // No message as in "Missing all"

    
})
