# Zadanie na pozíciu automation tester - Bart.sk

Testy funkcie phoneNrsAreEqual() a E2E testy stránky [bart.sk/mam-zaujem-test](https://www.bart.sk/mam-zaujem-test)

## Required

[cypress](https://www.cypress.io/)

[jest](https://jestjs.io/)

## Usage

```bash
cypress run
npm test
```
