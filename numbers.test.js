const numbers = require('./numbers');

// Correct num1, num2= 0909 123 456 
test('Same number no spaces', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "0909123456")).toBeTruthy();
});

test('Same number spaces', () => {
    expect(numbers.phoneNrsAreEqual("0909 123 456", "0909 123 456")).toBeTruthy();
});

test('Same number with country code', () => {
    expect(numbers.phoneNrsAreEqual("+421909123456", "+421909123456")).toBeTruthy();
});

test('Same number with country code and spaces', () => {
    expect(numbers.phoneNrsAreEqual("+421 909 123 456", "+421 909 123 456")).toBeTruthy();
});

test('Same number with country code and parentheses', () => {
    expect(numbers.phoneNrsAreEqual("+(421)909123456", "+(421)909123456")).toBeTruthy();
});

test('Same number with country code 00', () => {
    expect(numbers.phoneNrsAreEqual("00421909123456", "00421909123456")).toBeTruthy();
});

test('Same number with country code 00 and slash', () => {
    expect(numbers.phoneNrsAreEqual("00421/909123456", "00421/909123456")).toBeTruthy();
});

test('Same number with country code 00 and spaces', () => {
    expect(numbers.phoneNrsAreEqual("00421 909 123 456", "00421 909 123 456")).toBeTruthy();
});

test('Same number with country code 00 and dashes', () => {
    expect(numbers.phoneNrsAreEqual("00421-909-123-456", "00421-909-123-456")).toBeTruthy();
});

test('Same number without country code', () => {
    expect(numbers.phoneNrsAreEqual("909123456", "909123456")).toBeTruthy();
});

test('Same number different format with spaces', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "0909 123 456")).toBeTruthy();
});

test('Same number different format with dashes', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "0909-123-456")).toBeTruthy();
});

test('Same number different format with country code', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+421909123456")).toBeTruthy();
});

test('Same number different format with country code and spaces', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+421 909 123 456")).toBeTruthy();
});

test('Same number different format with country code and parentheses', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+(421)909123456")).toBeTruthy();
});

test('Same number different format with country code, spaces and parentheses', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+(421) 909 123 456")).toBeTruthy();
});

test('Same number different format with country code and dashes', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+421-909-123-456")).toBeTruthy();
});

test('Same number different format with country code, dashes and parentheses', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+(421)-909-123-456")).toBeTruthy();
});

test('Same number different format with country code 00', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "00421909123456")).toBeTruthy();
});

test('Same number different format with country code 00 and slash', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "00421/909123456")).toBeTruthy();
});

test('Same number different format with country code 00 and spaces', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "00421 909 123 456")).toBeTruthy();
});

test('Same number different format with country code 00 and dashes', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "00421-909-123-456")).toBeTruthy();
});

// Incorrect num1= 0909 123 456 , num2= 0909 654 321

test('Diffrent number no spaces', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "0909654321")).toBeFalsy();
});

test('Diffrent number spaces', () => {
    expect(numbers.phoneNrsAreEqual("0909 123 456", "0909 654 321")).toBeFalsy();
});

test('Diffrent number with country code', () => {
    expect(numbers.phoneNrsAreEqual("+421909123456", "+421909654321")).toBeFalsy();
});

test('Diffrent number with country code and spaces', () => {
    expect(numbers.phoneNrsAreEqual("+421 909 123 456", "+421 909 654 321")).toBeFalsy();
});

test('Diffrent number with country code and parentheses', () => {
    expect(numbers.phoneNrsAreEqual("+(421)909123456", "+(421)909654321")).toBeFalsy();
});

test('Diffrent number with country code 00', () => {
    expect(numbers.phoneNrsAreEqual("00421909123456", "00421909654321")).toBeFalsy();
});

test('Diffrent number with country code 00 and slash', () => {
    expect(numbers.phoneNrsAreEqual("00421/909123456", "00421/909654321")).toBeFalsy();
});

test('Diffrent number with country code 00 and spaces', () => {
    expect(numbers.phoneNrsAreEqual("00421 909 123 456", "00421 909 654 321")).toBeFalsy();
});

test('Diffrent number with country code 00 and dashes', () => {
    expect(numbers.phoneNrsAreEqual("00421-909-123-456", "00421-909-654-321")).toBeFalsy();
});

test('Diffrent number without country code', () => {
    expect(numbers.phoneNrsAreEqual("909123456", "909654321")).toBeFalsy();
});

test('Diffrent number different format with spaces', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "0909 654 321")).toBeFalsy();
});

test('Diffrent number different format with dashes', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "0909-654-321")).toBeFalsy();
});

test('Diffrent number different format with country code', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+421909654321")).toBeFalsy();
});

test('Diffrent number different format with country code and spaces', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+421 909 654 321")).toBeFalsy();
});

test('Diffrent number different format with country code and parentheses', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+(421)909654321")).toBeFalsy();
});

test('Diffrent number different format with country code, spaces and parentheses', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+(421) 909 654 321")).toBeFalsy();
});

test('Diffrent number different format with country code and dashes', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+421-909-654-321")).toBeFalsy();
});

test('Diffrent number different format with country code, dashes and parentheses', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "+(421)-909-654-321")).toBeFalsy();
});

test('Diffrent number different format with country code 00', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "00421909654321")).toBeFalsy();
});

test('Diffrent number different format with country code 00 and slash', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "00421/909654321")).toBeFalsy();
});

test('Diffrent number different format with country code 00 and spaces', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "00421 909 654 321")).toBeFalsy();
});

test('Diffrent number different format with country code 00 and dashes', () => {
    expect(numbers.phoneNrsAreEqual("0909123456", "00421-909-654-321")).toBeFalsy();
});

// One of the numbers would always have same format despite me using different ones, as 
// it would come from the phone, where it doesn´t vary as much, as when a user inputs it trough a website.

// Function now passes all tests, if the number is originated in Slovakia